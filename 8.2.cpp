﻿#include <iostream>
#include <conio.h>
using namespace std;

int n, m, summin = INT_MAX, sum = 0, stringmin = 0;

int main()
{
    cout << "Enter the amount of matrix rows and columns (both naturals less than 100): ";
    cin >> n >> m;

    if ((n < 1) ||  (n>100) || ( m < 1) || (n>100))
        cout << "Error! The amount of rows or columns is incorrect";
    else
    {
        int** matrix;
        matrix = new int* [n];

        for (int i = 0; i < n; i++)
        {
            matrix[i] = new int[m];
        }

        cout << "Fill the matrix: \n";
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                cin >> matrix[i][j];
            }
        }
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                sum = sum + matrix[i][j];
            }
            if (summin > sum)
            {
                stringmin = i;
                summin = sum;
            }
            sum = 0;
        }
        for (int i = stringmin; i <= stringmin; i++)
        {
            for (int j = 0; j < m; j++)
            {
                matrix[i][j] = summin;
            }
        }
        cout << "The min sum is in the line: " << summin << endl;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                cout << matrix[i][j] << " ";
            }
            cout << endl;
        }
        delete[] matrix;
    }
    _getch();
    
}